package com.rmq.producer.service;

import com.rmq.producer.model.Employee;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class RabbitMQSender {

    @Autowired
    private AmqpTemplate amqpTemplate;

    public void send(Employee employee) {
        amqpTemplate.convertAndSend("javaInUse-direct-exchange", "javaInUse", employee);
        log.info("Send msg = " + employee);

    }
}
